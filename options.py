# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'options.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtWidgets, QtCore

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtCore.QCoreApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtCore.QCoreApplication.translate(context, text, disambig)


class Ui_Options(object):
    def setupUi(self, Options):
        Options.setObjectName(_fromUtf8("Options"))
        Options.resize(420, 390)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Options.sizePolicy().hasHeightForWidth())
        Options.setSizePolicy(sizePolicy)
        Options.setMinimumSize(QtCore.QSize(420, 390))
        Options.setMaximumSize(QtCore.QSize(420, 390))
        Options.setStyleSheet(_fromUtf8(""))
        Options.setModal(True)
        self.save_button = QtWidgets.QPushButton(Options)
        self.save_button.setGeometry(QtCore.QRect(10, 352, 401, 31))
        self.save_button.setAutoFillBackground(False)
        self.save_button.setStyleSheet(_fromUtf8(""))
        self.save_button.setFlat(False)
        self.save_button.setObjectName(_fromUtf8("save_button"))
        self.delete_repo_button = QtWidgets.QPushButton(Options)
        self.delete_repo_button.setGeometry(QtCore.QRect(280, 280, 131, 23))
        self.delete_repo_button.setObjectName(_fromUtf8("delete_repo_button"))
        self.add_repo_button = QtWidgets.QPushButton(Options)
        self.add_repo_button.setGeometry(QtCore.QRect(10, 280, 131, 23))
        self.add_repo_button.setObjectName(_fromUtf8("add_repo_button"))
        self.edit_repo_button = QtWidgets.QPushButton(Options)
        self.edit_repo_button.setGeometry(QtCore.QRect(146, 280, 131, 23))
        self.edit_repo_button.setObjectName(_fromUtf8("edit_repo_button"))
        self.repo_list = QtWidgets.QTableWidget(Options)
        self.repo_list.setGeometry(QtCore.QRect(10, 10, 405, 261))
        self.repo_list.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.repo_list.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.repo_list.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.repo_list.setShowGrid(False)
        self.repo_list.setGridStyle(QtCore.Qt.DotLine)
        self.repo_list.setRowCount(0)
        self.repo_list.setColumnCount(4)
        self.repo_list.setObjectName(_fromUtf8("repo_list"))
        item = QtWidgets.QTableWidgetItem()
        self.repo_list.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.repo_list.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter | QtCore.Qt.AlignCenter)
        self.repo_list.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter | QtCore.Qt.AlignCenter)
        self.repo_list.setHorizontalHeaderItem(3, item)
        self.repo_list.horizontalHeader().setHighlightSections(False)
        self.repo_list.verticalHeader().setVisible(False)
        self.repo_list.verticalHeader().setHighlightSections(False)
        self.timer_checkbox = QtWidgets.QCheckBox(Options)
        self.timer_checkbox.setGeometry(QtCore.QRect(20, 310, 91, 17))
        self.timer_checkbox.setChecked(True)
        self.timer_checkbox.setObjectName(_fromUtf8("timer_checkbox"))
        self.browser_checkbox = QtWidgets.QCheckBox(Options)
        self.browser_checkbox.setGeometry(QtCore.QRect(20, 330, 131, 17))
        self.browser_checkbox.setChecked(True)
        self.browser_checkbox.setObjectName(_fromUtf8("browser_checkbox"))
        self.timer_edit = QtWidgets.QLineEdit(Options)
        self.timer_edit.setGeometry(QtCore.QRect(250, 320, 113, 20))
        self.timer_edit.setObjectName(_fromUtf8("timer_edit"))
        self.label = QtWidgets.QLabel(Options)
        self.label.setGeometry(QtCore.QRect(187, 322, 61, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtWidgets.QLabel(Options)
        self.label_2.setGeometry(QtCore.QRect(367, 323, 46, 13))
        self.label_2.setObjectName(_fromUtf8("label_2"))

        self.retranslateUi(Options)
        QtCore.QMetaObject.connectSlotsByName(Options)
        Options.setTabOrder(self.repo_list, self.add_repo_button)
        Options.setTabOrder(self.add_repo_button, self.edit_repo_button)
        Options.setTabOrder(self.edit_repo_button, self.delete_repo_button)
        Options.setTabOrder(self.delete_repo_button, self.timer_checkbox)
        Options.setTabOrder(self.timer_checkbox, self.browser_checkbox)
        Options.setTabOrder(self.browser_checkbox, self.save_button)

    def retranslateUi(self, Options):
        Options.setWindowTitle(_translate("Options", "Options", None))
        self.save_button.setText(_translate("Options", "Save", None))
        self.delete_repo_button.setText(_translate("Options", "Delete", None))
        self.add_repo_button.setText(_translate("Options", "Add new", None))
        self.edit_repo_button.setText(_translate("Options", "Edit", None))
        item = self.repo_list.horizontalHeaderItem(0)
        item.setText(_translate("Options", "Account", None))
        item = self.repo_list.horizontalHeaderItem(1)
        item.setText(_translate("Options", "Repository", None))
        item = self.repo_list.horizontalHeaderItem(2)
        item.setText(_translate("Options", "Credentials", None))
        item = self.repo_list.horizontalHeaderItem(3)
        item.setText(_translate("Options", "Active", None))
        self.timer_checkbox.setText(_translate("Options", "Enable Timer", None))
        self.browser_checkbox.setText(_translate("Options", "Open browser on click", None))
        self.label.setText(_translate("Options", "Check every", None))
        self.label_2.setText(_translate("Options", "minutes", None))
