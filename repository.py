# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'repository.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtWidgets, QtCore

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtCore.QCoreApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtCore.QCoreApplication.translate(context, text, disambig)


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(314, 212)
        Dialog.setMinimumSize(QtCore.QSize(314, 212))
        Dialog.setMaximumSize(QtCore.QSize(314, 212))
        self.option_repo_label = QtWidgets.QLabel(Dialog)
        self.option_repo_label.setGeometry(QtCore.QRect(6, 65, 101, 16))
        self.option_repo_label.setStyleSheet(_fromUtf8(""))
        self.option_repo_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.option_repo_label.setObjectName(_fromUtf8("option_repo_label"))
        self.repo_active = QtWidgets.QCheckBox(Dialog)
        self.repo_active.setGeometry(QtCore.QRect(22, 150, 101, 20))
        self.repo_active.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.repo_active.setChecked(True)
        self.repo_active.setObjectName(_fromUtf8("repo_active"))
        self.repo_label = QtWidgets.QLabel(Dialog)
        self.repo_label.setGeometry(QtCore.QRect(10, 9, 291, 20))
        self.repo_label.setAlignment(QtCore.Qt.AlignCenter)
        self.repo_label.setObjectName(_fromUtf8("repo_label"))
        self.password_line_edit = QtWidgets.QLineEdit(Dialog)
        self.password_line_edit.setGeometry(QtCore.QRect(110, 124, 191, 20))
        self.password_line_edit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_line_edit.setObjectName(_fromUtf8("password_line_edit"))
        self.repository_name_line_edit = QtWidgets.QLineEdit(Dialog)
        self.repository_name_line_edit.setGeometry(QtCore.QRect(110, 64, 191, 20))
        self.repository_name_line_edit.setObjectName(_fromUtf8("repository_name_line_edit"))
        self.username_label = QtWidgets.QLabel(Dialog)
        self.username_label.setGeometry(QtCore.QRect(4, 94, 101, 20))
        self.username_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.username_label.setObjectName(_fromUtf8("username_label"))
        self.username_line_edit = QtWidgets.QLineEdit(Dialog)
        self.username_line_edit.setGeometry(QtCore.QRect(110, 94, 191, 20))
        self.username_line_edit.setObjectName(_fromUtf8("username_line_edit"))
        self.password_label = QtWidgets.QLabel(Dialog)
        self.password_label.setGeometry(QtCore.QRect(4, 125, 101, 20))
        self.password_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.password_label.setObjectName(_fromUtf8("password_label"))
        self.account_name_label = QtWidgets.QLabel(Dialog)
        self.account_name_label.setGeometry(QtCore.QRect(6, 36, 101, 16))
        self.account_name_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.account_name_label.setObjectName(_fromUtf8("account_name_label"))
        self.account_name_line_edit = QtWidgets.QLineEdit(Dialog)
        self.account_name_line_edit.setGeometry(QtCore.QRect(110, 34, 191, 20))
        self.account_name_line_edit.setObjectName(_fromUtf8("account_name_line_edit"))
        self.save_push_button = QtWidgets.QPushButton(Dialog)
        self.save_push_button.setGeometry(QtCore.QRect(10, 177, 291, 31))
        self.save_push_button.setAutoFillBackground(False)
        self.save_push_button.setStyleSheet(_fromUtf8(""))
        self.save_push_button.setFlat(False)
        self.save_push_button.setObjectName(_fromUtf8("save_push_button"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        Dialog.setTabOrder(self.account_name_line_edit, self.repository_name_line_edit)
        Dialog.setTabOrder(self.repository_name_line_edit, self.username_line_edit)
        Dialog.setTabOrder(self.username_line_edit, self.password_line_edit)
        Dialog.setTabOrder(self.password_line_edit, self.repo_active)
        Dialog.setTabOrder(self.repo_active, self.save_push_button)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Repository Options", None))
        self.option_repo_label.setText(_translate("Dialog", "Repository name:", None))
        self.repo_active.setText(_translate("Dialog", "Active:", None))
        self.repo_label.setText(_translate("Dialog", "Bitbucket repositories (Press \"Add\" for new record)", None))
        self.username_label.setText(_translate("Dialog", "Username:", None))
        self.password_label.setText(_translate("Dialog", "Password:", None))
        self.account_name_label.setText(_translate("Dialog", "Account name:", None))
        self.save_push_button.setText(_translate("Dialog", "Save", None))
