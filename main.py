import sys
import os
from PyQt5 import QtWidgets, QtCore, QtGui
import webbrowser
import bitbucket
import json
import options
import base64
import repository
import logging
import time
import functools


def resource_path(relative):
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, relative)
    return os.path.join(relative)


tray_icon = None
config_file = resource_path('config.json')

try:
    config = json.load(open(config_file))
except (ValueError, IOError):
    config = {'repositories': [], 'open_browser': True, 'enable_timer': True, 'timer': 600000}
    json.dump(config, open(config_file, 'w'))

# TODO: CLEAN UP LOGGER
if not os.path.isdir('logs'):
    os.mkdir('logs')
log_filename = resource_path("logs/debug.log")
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.basicConfig(filename=log_filename, level=logging.INFO)


class MsgBox(QtWidgets.QWidget):
    def __init__(self):
        super(MsgBox, self).__init__()
        self.setGeometry(800, 500, 250, 100)
        self.setWindowTitle('New commit!')
        self.lbl = QtWidgets.QLabel('', self)
        self.lbl.move(15, 10)

    def closeEvent(self, event):
        self.hide()
        event.ignore()


class SystemTrayIcon(QtWidgets.QSystemTrayIcon):
    def __init__(self, icon, parent=None):
        QtWidgets.QSystemTrayIcon.__init__(self, icon, parent)
        self.app = parent
        self.dialog_opened = False
        self.dialog = None
        self.repository_ui = None
        self.queue = []
        self.messageConnected = False

        self.menu = QtWidgets.QMenu(parent)
        self.repo_menu = None

        repositories = config.get('repositories', [])

        self.repo_menu = QtWidgets.QMenu(self.app)
        self.repo_menu.setTitle("Repositories")
        self.menu.addMenu(self.repo_menu)

        if repositories:
            self.add_menu(repositories)

        # Add actions #
        check_action = self.menu.addAction("Check All")
        options_action = self.menu.addAction('Options')
        exit_action = self.menu.addAction("Exit")
        # Add connections #
        check_action.triggered.connect(self.reload_bitbucket)
        options_action.triggered.connect(self.open_options)
        exit_action.triggered.connect(QtWidgets.qApp.quit)

        self.setContextMenu(self.menu)
        enable_timer_setting = config.get('enable_timer')
        timer_seconds = config.get('timer', 600000)
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.reload_bitbucket)

        self.queue_timer = QtCore.QTimer(self)
        self.queue_timer.timeout.connect(self.release_queue)

        if enable_timer_setting:
            self.timer.start(timer_seconds)  # every 10 minutes

    def connect(self):
        self.reload_bitbucket(self)
    def add_menu(self, repositories):
        self.repo_menu.clear()
        for repo in repositories:
            menu_action = self.repo_menu.addAction("%s/%s" % (repo['account_name'], repo['repository']))
            self.rewire_menu_action(menu_action, repo)

    def rewire_menu_action(self, menu_action, repo):
        view_latest_function = functools.partial(self.view_latest, account=repo['account_name'],
                                                 repo=repo['repository'], commit=repo['latest_commit'])
        menu_action.triggered.connect(view_latest_function)

    def open_options(self):
        if not self.dialog_opened:
            parent_dialog = QtWidgets.QDialog()
            self.dialog = options.Ui_Options()
            self.dialog.setupUi(parent_dialog)
            self.dialog.parent = parent_dialog
            self.populate_list()  # Populate list
            self.dialog.save_button.clicked.connect(self.save_action)
            self.dialog.add_repo_button.clicked.connect(self.add_repo_action)
            self.dialog.edit_repo_button.clicked.connect(self.edit_repo_action)
            self.dialog.delete_repo_button.clicked.connect(self.delete_repo_action)
            self.dialog.timer_checkbox.setChecked(config.get('enable_timer', False))
            self.dialog.browser_checkbox.setChecked(config.get('open_browser', False))
            timer = config.get('timer')
            timer = int((timer / 60) / 1000)
            self.dialog.timer_edit.setText(str(timer))
            self.dialog_opened = True
            parent_dialog.exec_()
            self.dialog_opened = False

    def populate_list(self):
        repo_dict = config.get('repositories', [])
        self.dialog.repo_list.setRowCount(0)
        self.dialog.repo_list.setRowCount(len(repo_dict))
        count = 0
        for item in repo_dict:
            credentials_text = "No"
            active_text = "No"
            if item.get('auth'):
                credentials_text = "Yes"
            if item.get('active'):
                active_text = "Yes"
            self.dialog.repo_list.setItem(count, 0, QtWidgets.QTableWidgetItem(item['account_name']))
            self.dialog.repo_list.setItem(count, 1, QtWidgets.QTableWidgetItem(item['repository']))
            self.dialog.repo_list.setItem(count, 2, QtWidgets.QTableWidgetItem(credentials_text))
            self.dialog.repo_list.setItem(count, 3, QtWidgets.QTableWidgetItem(active_text))
            count += 1
        self.add_menu(repo_dict)

    def open_repository_options(self):
        parent_dialog = QtWidgets.QDialog()
        self.repository_ui = repository.Ui_Dialog()
        self.repository_ui.parent = parent_dialog
        self.repository_ui.setupUi(parent_dialog)

    def add_repo_action(self):
        self.open_repository_options()
        self.repository_ui.save_push_button.clicked.connect(self.save_new)
        self.repository_ui.parent.exec_()

    def edit_repo_action(self):
        repo_dict = config.get('repositories', [])
        current_row = self.dialog.repo_list.currentRow()
        if current_row >= 0:
            selected_repo = repo_dict[current_row]
            self.open_repository_options()
            # PRE-SET because of EDIT
            self.repository_ui.account_name_line_edit.setText(selected_repo.get('account_name', ''))
            self.repository_ui.repository_name_line_edit.setText(selected_repo.get('repository', ''))
            self.repository_ui.repo_active.setChecked(selected_repo.get('active', False))
            # END of PRE-SET
            self.repository_ui.save_push_button.clicked.connect(self.save_old)
            self.repository_ui.parent.exec_()

    def save_new(self):
        repo_dict = config.get('repositories', [])
        account_name = str(self.repository_ui.account_name_line_edit.text())
        repository_name = str(self.repository_ui.repository_name_line_edit.text())
        username = str(self.repository_ui.username_line_edit.text())
        password = str(self.repository_ui.password_line_edit.text())
        active = self.repository_ui.repo_active.checkState() == 2

        if account_name and repository_name:
            new_repo = {'account_name': account_name, 'repository': repository_name, 'active': active,
                        'latest_commit': ''}
            if password and username:
                auth_string = "%s:%s" % (str(username), str(password))
                encodedstring = self.stringToBase64(auth_string)
                new_repo['auth'] = encodedstring.decode('utf-8')
            repo_dict.append(new_repo)
            config['repositories'] = repo_dict
            json.dump(config, open(config_file, 'w'))
        self.populate_list()
        self.repository_ui.parent.close()

    def stringToBase64(self, s):
        return base64.b64encode(s.encode('utf-8'))

    def base64ToString(self, b):
        return base64.b64decode(b).decode('utf-8')

    def save_old(self):
        repo_dict = config.get('repositories', [])
        current_row = self.dialog.repo_list.currentRow()
        if current_row >= 0:
            selected_repo = repo_dict[current_row]
            old_auth = selected_repo.get('auth')

            account_name = str(self.repository_ui.account_name_line_edit.text())
            repository_name = str(self.repository_ui.repository_name_line_edit.text())
            username = str(self.repository_ui.username_line_edit.text())
            password = str(self.repository_ui.password_line_edit.text())
            active = self.repository_ui.repo_active.checkState() == 2

            if account_name and repository_name:
                new_repo = {'account_name': account_name, 'repository': repository_name, 'active': active,
                            'latest_commit': ''}
                if password and username:
                    auth_string = "%s:%s" % (str(username), str(password))
                    encodedstring = self.stringToBase64(auth_string)
                    new_repo['auth'] = encodedstring.decode('utf-8')
                else:
                    new_repo['auth'] = old_auth

                config['repositories'][current_row] = new_repo
                json.dump(config, open(config_file, 'w'))
        self.populate_list()
        self.repository_ui.parent.close()

    def delete_repo_action(self):
        repo_dict = config.get('repositories', [])
        current_row = self.dialog.repo_list.currentRow()
        if current_row >= 0:
            repositories = config.get('repositories', [])
            selected_repo = repo_dict[current_row]
            repositories.remove(selected_repo)
            json.dump(config, open(config_file, 'w'))
            self.populate_list()

    def save_action(self):
        config['enable_timer'] = self.dialog.timer_checkbox.checkState() == 2
        config['open_browser'] = self.dialog.browser_checkbox.checkState() == 2
        config['timer'] = (int(self.dialog.timer_edit.text()) * 60) * 1000

        # RESTART TIMER
        if config['enable_timer']:
            self.timer.stop()
            self.timer.start(config['timer'])
        else:
            self.timer.stop()

        json.dump(config, open(config_file, 'w'))
        self.dialog.parent.close()

    @staticmethod
    def save_new_commit(account_name, repo_name, commit_slug):
        existing_items = config.get('repositories', [])
        existing_items_enumerated = enumerate(existing_items)
        checked_key = False
        for item in existing_items_enumerated:
            if item[1]['account_name'] == account_name and item[1]['repository'] == repo_name:
                checked_key = item[0]

        config['repositories'][checked_key]['latest_commit'] = commit_slug
        json.dump(config, open(config_file, 'w'))

    def reload_bitbucket(self, reason=False):
        self.check_repo(reason)

    def check_repo(self, reason):
        acceptable_reasons = [4, 3, False]
        repositories = config.get('repositories', {})
        if reason in acceptable_reasons and repositories:
            for repo in repositories:
                account_name = repo.get('account_name')
                repo_name = repo.get('repository')
                latest_commit = repo.get('latest_commit')
                auth = repo.get('auth')
                active = repo.get('active')
                if active and account_name and repo_name and auth:
                    formatted_time = time.strftime('%d.%m.%Y %H:%M')
                    logging.info("[%s] Checking %s/%s!" % (formatted_time, account_name, repo_name))
                    api = bitbucket.API(auth)
                    changesets = api.get_changeset(account_name, repo_name)
                    commit_user = changesets['changesets'][0]['author']
                    commit_slug = changesets['changesets'][0]['raw_node']
                    if commit_slug != latest_commit:
                        self.save_new_commit(account_name, repo_name, commit_slug)
                        new_commit_subject = 'New commit in %s' % '/'.join((account_name, repo_name))
                        message = "Commit by %s" % commit_user
                        self.add_queue({'account': account_name, 'repo': repo_name, 'subject': new_commit_subject,
                                        'message': message, 'commit': commit_slug})
            self.release_queue()

    def go_to_commit(self, account, repo, commit):
        open_browser_setting = config.get('open_browser')
        if commit and account and repo:
            url = 'https://bitbucket.org/%s/%s/commits/%s' % (account, repo, commit)
            if url:
                if open_browser_setting:
                    webbrowser.open(url)
                else:
                    print("Opening browser on %s" % url)
            else:
                self.showMessage("No latest commit", "There was no latest commit detected.", 1)

    @staticmethod
    def view_latest(account, repo, commit):
        open_browser_setting = config.get('open_browser')
        if account and repo:
            url = 'https://bitbucket.org/%s/%s/commits/%s' % (account, repo, commit)
            if open_browser_setting:
                webbrowser.open(url)
            else:
                print("Opening browser on %s" % url)

    def show(self):
        QtWidgets.QSystemTrayIcon.show(self)

    def release_queue(self):
        if len(self.queue) > 0:
            subject = self.queue[0]['subject']
            message = self.queue[0]['message']
            commit = self.queue[0]['commit']
            repo = self.queue[0]['repo']
            account = self.queue[0]['account']
            tray_icon.showMessage(subject, message, 1)

            if self.messageConnected:
                tray_icon.messageClicked.disconnect()
            tray_icon.messageClicked.connect(
                functools.partial(self.go_to_commit, account=account, repo=repo, commit=commit))
            self.messageConnected = True
            self.remove_queue(self.queue[0])
            self.release_queue()

    def add_queue(self, queue):
        self.queue.append(queue)
        if not self.queue_timer.isActive():
            self.queue_timer.start(10000)

    def remove_queue(self, queue):
        self.queue.remove(queue)
        if len(self.queue) == 0 and self.queue_timer.isActive():
            self.queue_timer.stop()


def main():
    global tray_icon
    app = QtWidgets.QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)

    icon = QtGui.QIcon(resource_path('bitbucket.ico'))  # need a icon
    icon.addFile(resource_path('256x256.png'), QtCore.QSize(256, 256))
    icon.addFile(resource_path('bitbucket.ico'), QtCore.QSize(128, 128))
    app.setWindowIcon(icon)

    tray_icon = SystemTrayIcon(icon)
    tray_icon.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
