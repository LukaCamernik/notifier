import http.cookiejar
import urllib.request
import json


class API:
    api_url = 'http://api.bitbucket.org/1.0/'

    def __init__(self, auth, proxy=None):
        self._auth = "Basic %s" % auth
        self._opener = self._create_opener(proxy)

    @staticmethod
    def _create_opener(proxy=None):
        cj = http.cookiejar.LWPCookieJar()
        cookie_handler = urllib.request.HTTPCookieProcessor(cj)
        if proxy:
            proxy_handler = urllib.request.ProxyHandler(proxy)
            opener = urllib.request.build_opener(cookie_handler, proxy_handler)
        else:
            opener = urllib.request.build_opener(cookie_handler)
        return opener

    def get_changeset(self, username, repository):
        query_url = self.api_url + 'repositories/%s/%s/changesets/?limit=1' % (username, repository)
        try:
            req = urllib.request.Request(query_url, None, {"Authorization": self._auth})
            handler = self._opener.open(req)
        except urllib.request.HTTPError as e:
            raise e
        return json.load(handler)
